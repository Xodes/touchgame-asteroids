DoubleV[] playerPoints = new DoubleV[5];
Object player = new Object(0);

Object[] bullets = new Object[20];

double FrameRate = 30.0;
double framePeriod = 1.0 / FrameRate;

int scale = 8;
boolean debugG = false;

PImage screenI = createImage(128, 32, RGB);
PGraphics screen = new PGraphics();
PGraphics screenD = new PGraphics();
int ww = 128;
int hh = 32;


void setup()
{

  size(128, 64);
  smooth(0);
  screen = createGraphics(128, 32);
  screen.smooth(0);
  screenD = createGraphics(128*scale, 32*scale);
  screenD.smooth(0);

  surface.setResizable(true);

  screenI.loadPixels();
  for (int i=0; i<ww*hh; i++)
  {
    int layer = i/ww;
    if ( (i + layer%2)%2 > 0)
      screenI.pixels[i] = color(0, 0, 255);
    else
      screenI.pixels[i] = color(0, 0, 0);
  }
  screenI.updatePixels();

  gameInit();

  screen.beginDraw();
  screen.image(screenI, 0, 0);
  screen.endDraw();

  frameRate((int)FrameRate);
}

void gameInit()
{
  for (int i=0; i<20; i++)
  {
    bullets[i] = new Object(1);
    bullets[i].health = 0;
  }
  for (int i=0; i <5; i++)
  {
    playerPoints[i] = new DoubleV(0, 0);
  }
  playerPoints[0].setV(0, 1.4);
  playerPoints[1].setV(0, -1.4);
  playerPoints[2].setV(4.4, 0);
  playerPoints[3].setV(-1.4, -1.4);
  playerPoints[4].setV(-1.4, 1.4);

  player.pos = new DoubleV(32, 16);
}

void gameLoop()
{

  if (wPressed && !lastwPressed)
  {
    // fire
    int bulletCounter = 0;
    while (bullets[bulletCounter].health > 0)
    {
      bulletCounter++;
      if (bulletCounter >= 20)
      {
        //bulletCounter = 20;
        break;
      }
    }
    if (bulletCounter < 20)
    {
      bullets[bulletCounter] = new Object(1);
      bullets[bulletCounter].pos.setV(player.pos);
      bullets[bulletCounter].velocity = new DoubleV(60.0, 0).rotate(player.rot).add(player.velocity);
    }
  }

  for (int i=0; i<20; i++)
  {
    bullets[i].update();
    bullets[i].drawO(screen);
  }

  //keyPressedCheck
  // player.rotSpeed = 2*PI/20/10;
  player.update();
  player.drawO(screen);
  //player.rot += 2*PI/20/10;


  lastwPressed = wPressed;
}

void draw()
{
  surface.setSize(ww*scale, hh*scale);
  background(0);
  screenD.beginDraw();
  screenD.clear();
  screenD.endDraw();

  screen.beginDraw();
  screen.background(0);
  screen.endDraw();

  gameLoop();

  image(screen, 0, 0, ww*scale, hh*scale);
  image(screenD, 0, 0);
}


boolean rightPressed = false;
boolean leftPressed = false;
boolean wPressed = false, lastwPressed = false;
boolean sPressed = false;

void keyPressed()
{
  switch(keyCode)
  {
  case RIGHT:
    rightPressed = true;
    break;
  case LEFT:
    leftPressed = true;
    break;
  }
  switch(key)
  {
  case 'W':
  case 'w':
    wPressed = true;
    break;
  case 'S':
  case 's':
    sPressed = true;
    break;
  }
}

void keyReleased()
{
  switch(keyCode)
  {
  case RIGHT:
    rightPressed = false;
    break;
  case LEFT:
    leftPressed = false;
    break;
  }
  switch(key)
  {

  case 'W':
  case 'w':
    wPressed = false;
    break;
  case 'S':
  case 's':
    sPressed = false;
    break;
  }
}
