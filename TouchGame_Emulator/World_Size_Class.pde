//i somhow need to make a struct that either returns a PVector[2] or an int[2]


/* class ERect<T>
 {
 ERect(boolean CenterMode) 
 ERect( T inx, T iny, T inw, T inh) // defaults to cornerMode (centerMode = false)  
 ERect( T inx, T iny, T inw, T inh, boolean CenterMode) // this allows you to enter in the center x/y and change centerMode.   
 void setCenterMode(boolean CenterMode)  
 boolean CenterMode()
 
 // these 4 functions return the coordinate of one of the 4 walls.
 T left() 
 T right()  
 T top()  
 T bottom()
 
 // these 4 functions move the box so the coordinate matches of one of the 4 walls.
 void setLeft(T leftVal)
 void setRight(T rightVal)
 void setTop(T topVal)
 void setBottom(T bottomVal)
 
 // these 4 functions stretches the box so the coordinate matches of one of the 4 walls.
 // void stretchLeft(T leftVal)
 // void stretchRight(T rightVal)
 // void stretchTop(T topVal)
 // void stretchBottom(T bottomVal)   
 
 void setCenter(T inx, T iny) // input a center value without changing centerMode
 void setCorner(T inx, T iny) // input a topLeft corner value without changing centerMode
 
 // maxs a rect within these bounds. i guess it is similar to corner to corner.
 void setBounds(T inx1, T iny1, T inx2, T iny2)
 
 // https://stackoverflow.com/questions/13911188/java-generic-arithmetic
 T add(T t1, T t2) 
 T sub(T t1, T t2) 
 T half(T t1) 
 T divide(T t1, T t2) 
 T zero(T t1) 
 T two(T t1) 
 T absT(T t1) 
 T minT(T t1, T t2) 
 T maxT(T t1, T t2)
 
 // Maybe add (is box1 inside box2) ?
 
 } */

class MT<T> // it is a Math class that uses generic T. it is an MT class!
{
  MT() {
  }



  // https://stackoverflow.com/questions/13911188/java-generic-arithmetic
  T add(T t1, T t2) 
  {
    if (t1 instanceof Double) {
      return (T) Double.valueOf((double)t1 + (double)t2);
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf((float)t1 + (float)t2);
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf((int)t1 + (int)t2);
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  // https://stackoverflow.com/questions/13911188/java-generic-arithmetic
  T sub(T t1, T t2) 
  {
    if (t1 instanceof Double) {
      return (T) Double.valueOf((double)t1 - (double)t2);
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf((float)t1 - (float)t2);
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf((int)t1 - (int)t2);
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  // https://stackoverflow.com/questions/13911188/java-generic-arithmetic
  T half(T t1) 
  {
    if (t1 instanceof Double) {
      return (T) Double.valueOf((double)t1 / (double)2);
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf((float)t1 / (float)2);
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf((int)t1 / (int)2);
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T divide(T t1, T t2) 
  {
    if (t1 instanceof Double) {
      return (T) Double.valueOf((double)t1 / (double)t2);
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf((float)t1 / (float)t2);
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf((int)t1 / (int)t2);
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T zero(T t1) 
  {
    if (t1 instanceof Double) {
      return (T) Double.valueOf((double)0);
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf((float)0);
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf((int)0);
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T two(T t1) 
  {
    if (t1 instanceof Double) {
      return (T) Double.valueOf((double)2);
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf((float)2);
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf((int)2);
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T absT(T t1) 
  {
    if (t1 instanceof Double) {
      if ( abs( Float.valueOf((float)t1) ) / Float.valueOf((float)t1) < 0 ) // if it is negative
        return (T) Double.valueOf((double)t1 * (double)-1); // multiply by -1 to make it positive
      else
        return (T) Double.valueOf((double)t1); // else just return the already posotive number
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf(abs((float)t1));
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf(abs((int)t1));
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T minT(T t1, T t2) 
  {
    if (t1 instanceof Double) {
      if (Double.valueOf((double)t1 ) > Double.valueOf((double)t2 ) )
        return t2;
      else
        return t1;
    } else if (t1 instanceof Float) {
      if (Float.valueOf((float)t1 ) > Float.valueOf((float)t2 ) )
        return t2;
      else
        return t1;
    } else if (t1 instanceof Integer) {
      if (Integer.valueOf((int)t1 ) > Integer.valueOf((int)t2 ) )
        return t2;
      else
        return t1;
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T maxT(T t1, T t2) 
  {
    if (t1 instanceof Double) {
      if (Double.valueOf((double)t1 ) > Double.valueOf((double)t2 ) )
        return t1;
      else
        return t2;
    } else if (t1 instanceof Float) {
      if (Float.valueOf((float)t1 ) > Float.valueOf((float)t2 ) )
        return t1;
      else
        return t2;
    } else if (t1 instanceof Integer) {
      if (Integer.valueOf((int)t1 ) > Integer.valueOf((int)t2 ) )
        return t1;
      else
        return t2;
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T setT(T t1) 
  {
    if (t1 instanceof Double) {
      return t1;
    } else if (t1 instanceof Float) {
      return t1;
    } else if (t1 instanceof Integer) {
      return t1;
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }

  T sqT(T t1) // Square (pow( T , 2 ); 
  {
    if (t1 instanceof Double) {
      return (T) Double.valueOf((double)t1 * (double)t1);
    } else if (t1 instanceof Float) {
      return (T) Float.valueOf((float)t1 * (float)t1);
    } else if (t1 instanceof Integer) {
      return (T) Integer.valueOf((int)t1 * (int)t1);
    }
    // you can add all types or throw an exception
    throw new IllegalArgumentException();
  }
}



class TVector<T>
{ // make  PVector Struct that
  public T x, y;
  MT<T> mT = new MT();

  TVector()
  {
    this.x = mT.zero(x);
    this.y = mT.zero(y);
  }
  TVector(T xx, T yy)
  { // should this be mT.set(xx)?
    this.x = xx;
    this.y = yy;
  }

  TVector get()
  {
    return new TVector(x, y);
  }
  //void set(TVector inT)
  //{
  //  // try playing with myClass.isAsaignableFrom(obj.getClass()) to see if we can get this to work
  //  x = mT.setT(inT.x6);
  //  y = mT.setT(inT.y);
  //}

  // #################################################################################################################################################################
  /*
 // public:
   double x = 0.0, y = 0.0;
   
   TVector()
   {
   x = 0.0;
   y = 0.0;
   }
   
   TVector(double xx, double yy)
   {
   x = xx;
   y = yy;
   }
   
   
   TVector setV(double xx, double yy)
   {
   x = xx;
   y = yy;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector setV(TVector inV)
   {
   x = inV.x;
   y = inV.y;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector getV()
   {
   return this; // will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector clone()
   {
   return TVector(x, y); // will return a copy of the vector
   }
   
   TVector add(double zz)
   {
   x += zz;
   y += zz;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector add(double xx, double yy)
   {
   x += xx;
   y += yy;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector add(TVector zz)
   {
   x += zz.x;
   y += zz.y;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   
   TVector sub(double zz)
   {
   x -= zz;
   y -= zz;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector sub(double xx, double yy)
   {
   x -= xx;
   y -= yy;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector sub(TVector zz)
   {
   x -= zz.x;
   y -= zz.y;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   
   TVector mult(double zz)
   {
   x *= zz;
   y *= zz;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector mult(double xx, double yy)
   {
   x *= xx;
   y *= yy;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector mult(TVector zz)
   {
   x *= zz.x;
   y *= zz.y;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   
   TVector divide(double zz)
   {
   x /= zz;
   y /= zz;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector divide(double xx, double yy)
   {
   x /= xx;
   y /= yy;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector divide(TVector zz)
   {
   x /= zz.x;
   y /= zz.y;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   
   double mag()
   {
   return sqrt( pow(x, 2) + pow(y, 2) );
   }
   
   TVector sign()
   {
   int xs, ys;
   if (x == 0)
   xs = 0;
   else if (x > 0)
   xs = 1;
   else
   xs = -1;
   
   if (y == 0)
   ys = 0;
   else if (y > 0)
   ys = 1;
   else
   ys = -1;
   
   return TVector(xs, ys);
   }
   int signX()
   {
   int xs;
   if (x >= -0.000001 && x <= 0.000001)
   xs = 0;
   else if (x > 0.0)
   xs = 1;
   else
   xs = -1;
   
   return xs;
   }
   int signY()
   {
   int ys;
   if (y >= -0.000001 && y <= 0.000001)
   ys = 0;
   else if (y > 0.0)
   ys = 1;
   else
   ys = -1;
   
   return ys;
   }
   
   TVector normalize()
   {
   double magnitude = mag();
   x /= magnitude;
   y /= magnitude;
   return  this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector normals()
   {
   double xNorm, yNorm;
   double magnitude = mag();
   xNorm = x / magnitude;
   yNorm = y / magnitude;
   return TVector(xNorm, yNorm);
   }
   TVector setNormals(double xx, double yy)
   {
   double magnitude = mag();
   x = xx * magnitude;
   y = yy * magnitude;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector setNormals(TVector zz)
   {
   double magnitude = mag();
   x = zz.normals().x * magnitude;
   y = zz.normals().y * magnitude;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector setMag(double zz)
   {
   TVector norms = normals();
   x = norms.x * zz;
   y = norms.y * zz;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   TVector setMag(TVector zz)
   {
   double magnitude = zz.mag();
   TVector norms = normals();
   x = norms.x * magnitude;
   y = norms.y * magnitude;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   
   
   TVector rotate(double angle) // rotates about the origion (0,0)
   {
   TVector tempV;
   tempV.x = x * cos(angle) - y * sin(angle);
   tempV.y = x * sin(angle) + y * cos(angle);
   x = tempV.x;
   y = tempV.y;
   return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
   }
   
   double getAngle() // gives you the tan Angle // gets you the arccos
   {
   double angle = acos(x / mag() );
   if (y < 0)
   angle *= -1;
   return angle;
   }
   
   TVector setAngle(double angle)
   {
   double tempMag = mag();
   x = tempMag * cos(angle);
   y = tempMag * sin(angle);
   return this;
   }
   
   
   
   // do i add a dist or lerp function? or do i do  vc=va.clone().sub(vb); ?
   // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   */
}


//template< typename T, byte ArraySize >
class DoubleV
{

  // public:
  double x = 0.0, y = 0.0;

  DoubleV()
  {
    x = 0.0;
    y = 0.0;
  }

  DoubleV(double xx, double yy)
  {
    x = xx;
    y = yy;
  }


  DoubleV setV(double xx, double yy)
  {
    x = xx;
    y = yy;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV setV(DoubleV inV)
  {
    x = inV.x;
    y = inV.y;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV getV()
  {
    return this; // will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV clone()
  {
    return new DoubleV(x, y); // will return a copy of the vector
  }

  DoubleV add(double zz)
  {
    x += zz;
    y += zz;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV add(double xx, double yy)
  {
    x += xx;
    y += yy;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV add(DoubleV zz)
  {
    x += zz.x;
    y += zz.y;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }

  DoubleV sub(double zz)
  {
    x -= zz;
    y -= zz;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV sub(double xx, double yy)
  {
    x -= xx;
    y -= yy;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV sub(DoubleV zz)
  {
    x -= zz.x;
    y -= zz.y;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }

  DoubleV mult(double zz)
  {
    x *= zz;
    y *= zz;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV mult(double xx, double yy)
  {
    x *= xx;
    y *= yy;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV mult(DoubleV zz)
  {
    x *= zz.x;
    y *= zz.y;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }

  DoubleV divide(double zz)
  {
    x /= zz;
    y /= zz;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV divide(double xx, double yy)
  {
    x /= xx;
    y /= yy;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV divide(DoubleV zz)
  {
    x /= zz.x;
    y /= zz.y;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }

  double mag()
  {
    return sqrt((float)(x*x + y*y));
  }

  DoubleV sign()
  {
    int xs, ys;
    if (x == 0)
      xs = 0;
    else if (x > 0)
      xs = 1;
    else
      xs = -1;

    if (y == 0)
      ys = 0;
    else if (y > 0)
      ys = 1;
    else
      ys = -1;

    return new DoubleV(xs, ys);
  }
  int signX()
  {
    int xs;
    if (x >= -0.000001 && x <= 0.000001)
      xs = 0;
    else if (x > 0.0)
      xs = 1;
    else
      xs = -1;

    return xs;
  }
  int signY()
  {
    int ys;
    if (y >= -0.000001 && y <= 0.000001)
      ys = 0;
    else if (y > 0.0)
      ys = 1;
    else
      ys = -1;

    return ys;
  }

  DoubleV normalize()
  {
    double magnitude = mag();
    x /= magnitude;
    y /= magnitude;
    return  this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV normals()
  {
    double xNorm, yNorm;
    double magnitude = mag();
    xNorm = x / magnitude;
    yNorm = y / magnitude;
    return new DoubleV(xNorm, yNorm);
  }
  DoubleV setNormals(double xx, double yy)
  {
    double magnitude = mag();
    x = xx * magnitude;
    y = yy * magnitude;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV setNormals(DoubleV zz)
  {
    double magnitude = mag();
    x = zz.normals().x * magnitude;
    y = zz.normals().y * magnitude;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV setMag(double zz)
  {
    DoubleV norms = normals();
    x = norms.x * zz;
    y = norms.y * zz;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }
  DoubleV setMag(DoubleV zz)
  {
    double magnitude = zz.mag();
    DoubleV norms = normals();
    x = norms.x * magnitude;
    y = norms.y * magnitude;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }


  DoubleV rotate(double angle) // rotates about the origion (0,0)
  {
    DoubleV tempV = new DoubleV();
    tempV.x = x * cos((float)angle) - y * sin((float)angle);
    tempV.y = x * sin((float)angle) + y * cos((float)angle);
    x = tempV.x;
    y = tempV.y;
    return this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
  }

  double getAngle() // gives you the tan Angle // gets you the arccos
  {
    double angle = acos((float)(x / mag() ));
    if (y < 0)
      angle *= -1;
    return angle;
  }

  DoubleV setAngle(double angle)
  {
    double tempMag = mag();
    x = tempMag * cos((float)angle);
    y = tempMag * sin((float)angle);
    return this;
  }


  DoubleV roundV()
  {
    //x = round((float)x);
    //y = round((float)y);
    long intX = (long)(x + 0.5);
    long intY = (long)(y + 0.5);
    x = (double)intX;
    y = (double)intY;
    return this;
  }

  DoubleV ceilV()
  {
    //x = round((float)x);
    //y = round((float)y);
    long intX = (long)(x);
    long intY = (long)(y);

    if (x > (double)intX)
      intX ++;
    x = (double)intX;

    if (y > (double)intY)
      intY ++;
    y = (double)intY;
    return this;
  }

  DoubleV floorV()
  {
    //x = round((float)x);
    //y = round((float)y);
    long intX = (long)(x);
    long intY = (long)(y);
    x = (double)intX;
    y = (double)intY;
    return this;
  }

  // do i add a dist or lerp function? or do i do  vc=va.clone().sub(vb); ?
}


double roundD(double inNum)
{
  long intNum = (long)(inNum + 0.5);
  inNum = (double)intNum;
  return inNum;
}

double ceilD(double inNum)
{
  long intNum = (long)(inNum + 0.5);
  if (inNum > (double)intNum)
    intNum ++;
  inNum = (double)intNum;
  return inNum;
}

double floorD(double inNum)
{
  long intNum = (long)(inNum);
  inNum = (double)intNum;
  return inNum;
}



class ERect<T>
{
  public T x, y, w, h; 
  private boolean centerMode = false; // sets how x and y are stored. changing this with setCenterMode() will alter these.
  MT<T> mT = new MT();

  ERect(boolean CenterMode)
  {
    this.centerMode = CenterMode;

    this.x = mT.zero(x);
    this.y = mT.zero(y);
    this.w = mT.zero(w);
    this.h = mT.zero(h);
  }
  ERect( T inx, T iny, T inw, T inh) // defaults to cornerMode (centerMode = false)
  {
    this.centerMode = false;

    this.x = inx;
    this.y = iny;
    this.w = inw;
    this.h = inh;
  }
  ERect( T inx, T iny, T inw, T inh, boolean CenterMode) // this allows you to enter in the center x/y and change centerMode. 
  {
    this.centerMode = CenterMode;

    this.w = inw;
    this.h = inh;
    this.x = inx;
    this.y = iny;
  }

  void setCenterMode(boolean CenterMode)
  {
    if (centerMode != CenterMode)
    { // only alter x,y if there is a change in state
      if (CenterMode) // if we are changing from topLeft to center...
      {
        x = mT.add(x, mT.half(w));
        y = mT.add(y, mT.half(h));
      } else // if we are changing from center to topLeft...
      {
        x = mT.sub(x, mT.half(w));
        y = mT.sub(y, mT.half(h));
      }
      centerMode = CenterMode;
    }
  }

  boolean CenterMode()
  {
    return centerMode;
  }

  // these 4 functions return the coordinate of one of the 4 walls.
  T left()
  {
    if (centerMode)
    {
      return mT.sub(x, mT.half(w));
    } else
    {
      return x;
    }
  }

  T right()
  {
    if (centerMode)
    {
      return mT.add(x, mT.half(w));
    } else
    {
      return mT.add(x, w);
    }
  }

  T top()
  {
    if (centerMode)
    {
      return mT.sub(y, mT.half(h));
    } else
    {
      return y;
    }
  }

  T bottom()
  {
    if (centerMode)
    {
      return mT.add(y, mT.half(h));
    } else
    {
      return mT.add(y, h);
    }
  }


  // these 4 functions move the box so the coordinate matches of one of the 4 walls.
  void setLeft(T leftVal)
  {
    if (centerMode)
    {
      x = mT.add(leftVal, mT.half(w));
    } else
    {
      x = leftVal;
    }
  }

  void setRight(T rightVal)
  {
    if (centerMode)
    {
      x = mT.sub(rightVal, mT.half(w));
    } else
    {
      x = mT.sub(rightVal, w);
    }
  }

  void setTop(T topVal)
  {
    if (centerMode)
    {
      y = mT.sub(topVal, mT.half(h));
    } else
    {
      y = topVal;
    }
  }

  void setBottom(T bottomVal)
  {
    if (centerMode)
    {
      y = mT.add(bottomVal, mT.half(h));
    } else
    {
      y = mT.add(bottomVal, h);
    }
  }

  /*
  // these 4 functions stretches the box so the coordinate matches of one of the 4 walls.
   void stretchLeft(T leftVal)
   {
   T lastRight = right();
   if (centerMode)
   {
   w = absT(sub(right(), leftVal));
   x = add(minT(leftVal, lastRight), half(w));
   } else
   {
   T lastX = x;
   x = minT(leftVal, right());
   w = absT(add(w, sub(lastX, x)));
   }
   }
   
   void stretchRight(T rightVal)
   {
   if (centerMode)
   {
   w = absT(sub(rightVal, left()));
   x = sub(rightVal, half(w));
   } else
   {
   w = absT(sub(rightVal, x));
   }
   }
   
   void stretchTop(T topVal)
   {
   if (centerMode)
   {
   h = absT(sub(bottom(), topVal));
   x = add(leftVal, half(w));
   } else
   {
   T lastY = y;
   y = topVal;
   h = absT(add(h, sub(lastY, y)));
   }
   }
   
   void stretchBottom(T bottomVal)
   {
   if (centerMode)
   {
   y = add(bottomVal, half(h));
   } else
   {
   y = add(bottomVal, h);
   }
   }
   */





  void setCenter(T inx, T iny) // input a center value without changing centerMode
  {
    if (centerMode)
    {
      x = inx;
      y = iny;
    } else
    {
      x = mT.sub(inx, mT.half(w));
      y = mT.sub(iny, mT.half(h));
    }
  }
  TVector getCenter() // returns a center value without changing centerMode
  {
    if (centerMode)
    {
      return new TVector(x, y);
    } else
    {
      return new TVector(mT.add(x, mT.half(w)), mT.add(y, mT.half(h)));
    }
  }

  void setCorner(T inx, T iny) // input a topLeft corner value without changing centerMode
  {
    if (centerMode)
    {
      x = mT.add(x, mT.half(w));
      y = mT.add(y, mT.half(h));
    } else
    {
      x = inx;
      y = iny;
    }
  }
  TVector getCorner() // returns a topLeft corner value without changing centerMode
  {
    if (centerMode)
    {
      return new TVector(mT.sub(x, mT.half(w)), mT.sub(y, mT.half(h)) );
    } else
    {
      return new TVector(x, y);
    }
  }


  // maxs a rect within these bounds. i guess it is similar to corner to corner.
  void setBounds(T inx1, T iny1, T inx2, T iny2) // Left, Top, Right, Bottom
  {
    if (centerMode)
    {
      x = mT.half(mT.add(inx1, inx2));
      y = mT.half(mT.add(iny1, iny2));
      w = mT.absT(mT.sub(inx1, inx2));
      h = mT.absT(mT.sub(iny1, iny2));
    } else
    {
      x = mT.minT(inx1, inx2);
      y = mT.minT(iny1, iny2);
      w = mT.absT(mT.sub(inx1, inx2));
      h = mT.absT(mT.sub(iny1, iny2));
    }
  }


  void setAgain( T inx, T iny, T inw, T inh, boolean CenterMode) // set the box the same way as you would when initializing. CenterMode does not set centerMode, only the way the numbers are interpreted.
  {
    w = inw;
    h = inh;

    if (CenterMode)
      setCenter(inx, iny);
    else
      setCorner(inx, iny);
  }
}












class WorldSize
{
  // float pxPin = 1.0; // Pixels Per world inch. (Horizontally)
  //float aspectRatio = 1.0; // How many more VerticalinchesPerPixel there are than HorizontalinchesPerPixel. (bigger numbers will squish things down vertially, but display more vertical inches onScreen)

  // i thought i might also have to change some stored coordinates, Not sure. i suppose you can use a negative aspectRatio to do the same thing as invertY.
  private PVector invert = new PVector(1.0, 1.0); // inVert X and Y. // 1.0 = the world inch Y is the same orientation as the screen coordinates. -1.0 = the world inch Y is inverted, to match cartesian coordinate orientation.

  // private float pxPin = new PVector(10, 10); // Pixels Per world inch. (Horizontally, Vertically). if V is negative, posotive inches means a upward direction.

  //PVector wpOffset = new PVector(0, 0); // World Pixel Offset. this is the offset for the center of the pixel world to the top left corner of the screen in pixels.
  //PVector wiOffset = new PVector(0, 0); // World inch Offset. this is the offset for the center of the pixel world to the top left corner of the screen in pixels.
  // int screenWidth = 200, screenHeight = 200; // this class will remember our set screenWidth and screenHeight. we make no assumptions of if the user wants these variables to be the same as the set window size.
  // PVector screenCenter = new PVector(screenWidth/2, screenHeight/2);
  //ERect <Integer> screenP = new ERect(wpOffset.x, wpOffset.y, 200, 200); // makes a rectagle representing the screen in world pixels, the topLeft corner of the screen is at (0,0).
  ERect <Integer> screenPos = new ERect(0, 0, 200, 200); // makes a rectagle representing the display screen in pixels, the topLeft corner of the screen is at (0,0). This is  
  ERect <Integer> screenP = new ERect(0, 0, screenPos.w, screenPos.h); // makes a rectagle representing the screen in world pixels, the topLeft corner of the screen is at (0,0).
  // ERect <Float> screeni = new ERect(wiOffset.x, wiOffset.y, l2i(int(screenP.w)), l2i(int(screenP.h)) * aspectRatio); // makes a rectagle representing the screen in world inches, the topLeft corner of the screen is at (0,0).
  ERect <Float> screeni = new ERect(0.0, 0.0, 20.0, 20.0, true); // makes a rectagle representing the screen in world inches, the topLeft corner of the screen is at (0,0).





  WorldSize(float pixelsPerinch, float aspect_ratio, float topLeftScreenX, float topLeftScreenY)
  {
  }

  WorldSize(int screenWidth, int screenHeight, float topLeftScreenX, float topLeftScreenY)
  {
  }

  WorldSize()
  {
  }

  /*
  // Scales a legnth from Pixels to inches.
   //float pxLengthToin(long pxLength)
   float l2i(long pxLength) 
   {
   float tempLength = pxLength;
   return tempLength / pxPinH;
   }
   // Scales a legnth from inches to Pixels.
   //long inLengthToPx(float inLength)
   long l2p(float inLength) 
   {
   float tempLength = inLength * pxPinH;
   long outLength = (long)(tempLength);
   return outLength;
   }
   */

  // translates a Position from world pixels to world inches
  PVector wp2wi(TVector<Integer> worldPixelPos)
  {
    PVector worlinchPos = new PVector(worldPixelPos.x * invert.x * screeni.w / screenP.w, worldPixelPos.y * invert.y * screeni.h / screenP.h);
    return worlinchPos;
  }
  // translates a Position from world inches to world pixels
  TVector<Integer> wi2wp(PVector worldinchPos)
  {
    TVector<Integer> worldPixelPos = new TVector<Integer>(int(worldinchPos.x * invert.x * screenP.w/screeni.w), int(worldinchPos.y * invert.y * screenP.h/screeni.h));
    return worldPixelPos;
  }

  boolean getinvertY()
  {
    return invert.y<0?true:false; // if we are inverted (== -1), return true
  }
  void setinvertY(boolean inverted) // i thought i might also have to change some stored coordinates, Not sure. i suppose you can use a negative aspectRatio to do the same thing as invertY.
  {
    if (inverted)
    {
      invert.y = -1.0;
    } else
    {
      invert.y = 1.0;
    }
  }
  boolean getinvertX()
  {
    return invert.x<0?true:false; // if we are inverted (== -1), return true
  }
  void setinvertX(boolean inverted) // i thought i might also have to change some stored coordinates, Not sure. i suppose you can use a negative aspectRatio to do the same thing as invertY.
  {
    if (inverted)
    {
      invert.x = -1.0;
    } else
    {
      invert.x = 1.0;
    }
  }


  // converts a screen x/y pixel coordinate to a world x/y pixel coordinate
  TVector<Integer> screen2worldP(TVector<Integer> screenPixelPos)
  {
    TVector<Integer> worldPixelPos = new TVector<Integer>(screenPixelPos.x - screenPos.left() + screenP.left(), screenPixelPos.y - screenPos.top() + screenP.top());
    return worldPixelPos;
  }  
  // converts a world x/y pixel coordinate to a screen x/y pixel coordinate
  TVector<Integer> worldP2screen(TVector<Integer> worldPixelPos)
  {
    TVector<Integer> screenPixelPos = new TVector<Integer>(worldPixelPos.x - screenP.left() + screenPos.left(), worldPixelPos.y - screenP.top() + screenPos.top());
    return screenPixelPos;
  }  


  // converts a screen x/y pixel coordinate to a world x/y inch coordinate. input can be something like TVector<Integer>(mouseX, mouseY)
  PVector screen2worldi(TVector<Integer> screenPixelPos)
  {
    TVector<Integer> worldPixelPos = screen2worldP(screenPixelPos); 
    PVector worldinchPos = wp2wi(worldPixelPos);
    return worldinchPos;
  }
  // converts a world x/y inch coordinate to a screen x/y pixel coordinate
  TVector<Integer> worldi2screen(PVector worldinchPos)
  {
    TVector<Integer> worldPixelPos = wi2wp(worldinchPos); 
    TVector<Integer> screenPixelPos = worldP2screen(worldPixelPos);
    return screenPixelPos;
  }


  // sets the stored variables for remembering the screen size. they are used for knowing the center of the screen.
  // if you wanted to have a fixed number of inches per screen, this can optionally change those too.
  //void resizeScreen(int newScreenWidth, int newScreenHeight)
  //{
  //  screenWidth = newScreenWidth;
  //  screenHeight = newScreenHeight;

  //  screenCenter.x = screenWidth/2;
  //  screenCenter.y = screenHeight/2;
  //}




  // ERect <Float> screeni = new ERect(wiOffset.x, wiOffset.y, l2i(int(screenP.w)), l2i(int(screenP.h)) * aspectRatio); // makes a rectagle representing the screen in world inches, the topLeft corner of the screen is at (0,0).
  void setScreenP() // change screenPixel to match screeninch
  {// ScreenPixel.w and .h will stay the same. what changes is (ScreenP.x, screenP.y). 
    // screenP.setCenterMode(true);
    // screeni.setCenterMode(true);

    screenP.w = screenPos.w;
    screenP.h = screenPos.h;

    /*

     PVector temp = new PVector((float)screenP.w, (float)screenP.h);
     //PVector temp2 = new PVector(( (float)screeni.getCenter().x) * invert.x * (((float)screenP.w)/screeni.w), ((float)screeni.getCenter().y) * invert.y * (((float)screenP.h)/screeni.h));
     int temp3x = (int)( (float)screeni.getCenter().x);
     int temp3y = (int)( (float)screeni.getCenter().y);
     PVector temp4 = new PVector( (float) temp3x, (float) temp3y);
     
     PVector temp6 = new PVector((float)screeni.w, (float)screeni.h); // this is where we had issues. instead of setting it as   ERect <Float> screeni = new ERect(0, 0, 20, 20, true); we use   ERect <Float> screeni = new ERect(0.0, 0.0, 20.0, 20.0, true);
     PVector temp5 = new PVector( (float)temp.x/ temp6.x, (float)temp.y/ temp6.x);
     PVector temp2 = new PVector(temp4.x * invert.x * temp5.x, temp4.y * invert.y * temp5.y);
     
     //screenP.x = (int)(screeni.x * invert.x * ((temp.x)/screeni.w));
     //screenP.y = (int)(screeni.y * invert.y * ((temp.y)/screeni.h));
     //screenP.setCenter( (int)( (float)screeni.getCenter().x * invert.x * (((float)screenP.w)/screeni.w)), (int)( (float)screeni.getCenter().y * invert.y * (((float)screenP.h)/screeni.h)) );
     screenP.setCenter( (int)temp2.x, (int)temp2.y );
     */
    screenP.setCenter( (int)( (float)screeni.getCenter().x * invert.x * (((float)screenP.w)/screeni.w)), (int)( (float)screeni.getCenter().y * invert.y * (((float)screenP.h)/screeni.h)) );
  }


  //void setifromPBox(ERect PBox) // change screeninch to match a pixelBox drawn From the screen.
  //{
  //  // screenP.setCenterMode(true);
  //  // screeni.setCenterMode(true);

  //  screenP.w = screenPos.w;
  //  screenP.h = screenPos.h;

  //  screenP.x = (int)(screeni.x * (((float)screenP.w)/screeni.w));
  //  screenP.y = (int)(screeni.y * invertY * (((float)screenP.h)/screeni.h));
  //}

  //void setScreenP() // change screenPixel to match screeninch
  //{
  //  PVector worldinchCorner1 = new PVector(screeni.left(), screeni.top());
  //  PVector worldinchCorner2 = new PVector(screeni.right(), screeni.bottom());
  //  PVector worldPixelCorner1 = new PVector(screeni.left(), screeni.top());
  //  screenP.setBounds(
  //}

  void lineinch(float x1, float y1, float x2, float y2, PGraphics g)
  {
    TVector<Integer>  p1 = worldi2screen(new PVector(x1, y1));
    TVector<Integer>  p2 = worldi2screen(new PVector(x2, y2));
    g.line(p1.x, p1.y, p2.x, p2.y);
  }

  void lineinch(PVector ip1, PVector ip2, PGraphics g)
  {
    TVector<Integer>  p1 = worldi2screen(ip1);
    TVector<Integer>  p2 = worldi2screen(ip2);
    g.line(p1.x, p1.y, p2.x, p2.y);
  }

  void rectinch(float xx, float yy, float ww, float hh, PGraphics g)
  {
    TVector<Integer>  p1 = worldi2screen(new PVector(xx, yy));
    TVector<Integer>  p2 = wi2wp(new PVector(ww, hh));
    g.rect(p1.x, p1.y, p2.x, p2.y);
  }
  void ellipseinch(float xx, float yy, float ww, float hh, PGraphics g)
  {
    TVector<Integer>  p1 = worldi2screen(new PVector(xx, yy));
    TVector<Integer>  p2 = wi2wp(new PVector(ww, hh));
    g.ellipse(p1.x, p1.y, p2.x, p2.y);
  }
  void quadinch(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, PGraphics g)
  {
    TVector<Integer>  p1 = worldi2screen(new PVector(x1, y1));
    TVector<Integer>  p2 = worldi2screen(new PVector(x2, y2));
    TVector<Integer>  p3 = worldi2screen(new PVector(x3, y3));
    TVector<Integer>  p4 = worldi2screen(new PVector(x4, y4));
    g.quad(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y);
  }
}
