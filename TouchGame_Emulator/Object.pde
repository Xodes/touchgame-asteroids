class Object
{
  double rot = 0; // 0 rad = pointing to the right. more posotive is more clockwise.
  double rotSpeed = 0;
  DoubleV pos = new DoubleV();
  DoubleV velocity = new DoubleV();
  double health = 100;
  int objectType = -1;
  int objectVariant = -1;
  int objectClass = -1;
  Object(int inObjectType)
  {
    objectType = inObjectType;
  }

  void drawO(PGraphics g)
  {
    // objectType: -1 (inValid), 0 (player), 1 (bullet), 2 (asteroid)
    // objectVarient: -1 (invalid), 1 (style 1), 2 (style 2), 3 (style 3) ..

    switch(objectType)
    {
    case 0:
      //Draw a rotated T
      g.beginDraw();
      g.stroke(0, 0, 255);
      g.fill(0, 0, 255);
      //DoubleV tempV1 = new DoubleV(), tempV2 = new DoubleV(), tempV3 = new DoubleV();
      DoubleV[] tempV = new DoubleV[5];
      for (int i=0; i<5; i++)
      {
        tempV[i] = (playerPoints[i].clone().rotate(rot)).add(pos);
      }
      g.line((float)tempV[0].x, (float)tempV[0].y, (float)tempV[1].x, (float)tempV[1].y);
      //tempV1 = (playerPoints[2].clone().rotate(rot)).add(pos);
      // g.line((float)pos.x, (float)pos.y, (float)tempV[2].x, (float)tempV[2].y);
      g.line((float)tempV[0].x, (float)tempV[0].y, (float)tempV[2].x, (float)tempV[2].y);
      g.line((float)tempV[2].x, (float)tempV[2].y, (float)tempV[1].x, (float)tempV[1].y);
      g.line((float)tempV[0].x, (float)tempV[0].y, (float)tempV[4].x, (float)tempV[4].y);
      g.line((float)tempV[1].x, (float)tempV[1].y, (float)tempV[3].x, (float)tempV[3].y);
      g.endDraw();

      if (debugG)
      {
        screenD.beginDraw();
        screenD.stroke(255, 0, 255);
        screenD.fill(255, 0, 255);
        screenD.rectMode(CENTER);
        screenD.rect((float)tempV[2].x*scale, (float)tempV[2].y*scale, 1, 1);
        screenD.endDraw();
      }
      break;

    case 1: // bullet
      if (health>0)
      {
        g.beginDraw();
        g.stroke(0, 0, 255);
        g.fill(0, 0, 255);
        g.noStroke();
        g.rectMode(CENTER);
        g.rect((float)pos.x, (float)pos.y, 1, 1);
        g.endDraw();
      }

      break;
    }
  }


  void update()
  {
    rot += rotSpeed*framePeriod;
    pos.add(velocity.clone().mult(framePeriod));
    //pos.x = (pos.x + 8*ww) % ww;
    //pos.y = (pos.y + 8*hh) % hh;
    pos.x = fmod(pos.x + ww, ww);
    pos.y = fmod(pos.y + hh, hh);
    switch(objectType)
    {
    case 0: // player
      //keypresses
      if (rightPressed)
        rot += 2*PI/FrameRate/2.0;
      if (leftPressed)
        rot -= 2*PI/FrameRate/2.0;
      if (rightPressed && leftPressed)
      {
        double thrust = 3.33; //0.333; //2*3/(3*3) == a == 2/3
        DoubleV tempV = new DoubleV(thrust, 0);
        velocity.add(tempV.rotate(rot));
      }
      if (velocity.mag() > 128.0)
        velocity.setMag(128.0);
      break;
    case 1: // bullet
      if (health>0)
        health -= 1;
      break;
    }
    if (health>0)
      collisionCheck();
  }

  boolean collisionCheck()
  {
    boolean bool = false;
    return bool;
  }
}


double fmod(double inA, double inMod)
{
  while (inA >= inMod)
  {
    inA -= inMod;
  }
  return inA;
}
