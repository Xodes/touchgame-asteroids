#ifndef ObjectClass
#define ObjectClass 1

#ifndef DoubleVClass
#include "DoubleV.h"
#endif

DoubleV playerPoints[5];
//Object player = Object(0);


class Object
{
  public:
    double rot = 0; // 0 rad = pointing to the right. more posotive is more clockwise.
    double rotSpeed = 0;
    DoubleV pos;
    DoubleV velocity;
    double health = 100;
    int objectType = -1;
    int objectVariant = -1;
    int objectClass = -1;


    Object()
    {
      objectType = 1;

    }


    Object(int inObjectType)
    {
      objectType = inObjectType;

    }

    void draw()
    {
      // objectType: -1 (inValid), 0 (player), 1 (bullet), 2 (asteroid)
      // objectVarient: -1 (invalid), 1 (style 1), 2 (style 2), 3 (style 3) ..

      switch (objectType)
      {
        case 0:
          {
            //Draw a rotated T
            //          g.beginDraw();
            //          g.stroke(0, 0, 255);
            //          g.fill(0, 0, 255);

            //DoubleV tempV1 = new DoubleV(), tempV2 = new DoubleV(), tempV3 = new DoubleV();
            DoubleV tempV[5]; // = new DoubleV[5];
            for (int i = 0; i < 5; i++)
            {
              tempV[i] = (playerPoints[i].clone().rotate(rot)).add(pos);
            }

            display.drawLine((int)tempV[0].x, (int)tempV[0].y, (int)tempV[1].x, (int)tempV[1].y);
            //tempV1 = (playerPoints[2].clone().rotate(rot)).add(pos);
            // g.line((float)pos.x, (float)pos.y, (float)tempV[2].x, (float)tempV[2].y);
            display.drawLine((int)tempV[0].x, (int)tempV[0].y, (int)tempV[2].x, (int)tempV[2].y);
            display.drawLine((int)tempV[2].x, (int)tempV[2].y, (int)tempV[1].x, (int)tempV[1].y);
            display.drawLine((int)tempV[0].x, (int)tempV[0].y, (int)tempV[4].x, (int)tempV[4].y);
            display.drawLine((int)tempV[1].x, (int)tempV[1].y, (int)tempV[3].x, (int)tempV[3].y);

            //          g.line((float)tempV[0].x, (float)tempV[0].y, (float)tempV[1].x, (float)tempV[1].y);
            //          //tempV1 = (playerPoints[2].clone().rotate(rot)).add(pos);
            //          // g.line((float)pos.x, (float)pos.y, (float)tempV[2].x, (float)tempV[2].y);
            //          g.line((float)tempV[0].x, (float)tempV[0].y, (float)tempV[2].x, (float)tempV[2].y);
            //          g.line((float)tempV[2].x, (float)tempV[2].y, (float)tempV[1].x, (float)tempV[1].y);
            //          g.line((float)tempV[0].x, (float)tempV[0].y, (float)tempV[4].x, (float)tempV[4].y);
            //          g.line((float)tempV[1].x, (float)tempV[1].y, (float)tempV[3].x, (float)tempV[3].y);

            break;
          }
        case 1: // bullet
          {
            if (health > 0)
            {
              //            g.beginDraw();
              //            g.stroke(255, 0, 255);
              //            g.fill(255, 0, 255);
              //display.rectMode(CENTER);
              display.fillRect((int)pos.x * scale, (int)pos.y * scale, 1, 1);
              //g.endDraw();
            }

            break;
          }
      }
    }


    void update()
    {
      rot += rotSpeed * framePeriod;
      pos.add(velocity.clone().mult(framePeriod));
      pos.x = fmod(pos.x + ww, ww);
      pos.y = fmod(pos.y + hh, hh);
      switch (objectType)
      {
        case 0: // player
          //keypresses
          if (rightPressed)
            rot += 2 * PI / frameRate / 2.0;
          if (leftPressed)
            rot -= 2 * PI / frameRate / 2.0;
          if (rightPressed && leftPressed)
          {
            double thrust = 3.33; //0.333; //2*3/(3*3) == a == 2/3
            DoubleV tempV = DoubleV(thrust, 0);
            velocity.add(tempV.rotate(rot));
          }
          if (velocity.mag() > 128.0)
            velocity.setMag(128.0);
          break;
        case 1: // bullet
          if (health > 0)
            health -= 1;
          break;
      }
      if (health > 0)
        collisionCheck();
    }

    boolean collisionCheck()
    {
      //boolean bool = false;
      return false;
    }
};



double fmod(double inA, double inMod)
{
  while (inA >= inMod)
  {
    inA -= inMod;
  }
  return inA;
}

#endif
