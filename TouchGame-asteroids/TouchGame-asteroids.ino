// ESP32 Touch Test
// Just test touch pin - Touch0 is T0 which is on GPIO 4.

byte touchPin[] = {T0, T1, T2, T3, T4, T5, T6, T7, T8, T9};
byte touchValue[10];
boolean touched[10];

byte threshold = 60;

boolean rightPressed = false;
boolean leftPressed = false;
boolean wPressed = false, lastwPressed = false;
boolean sPressed = false;

double frameRate = 30.0;
double framePeriod = 1.0 / frameRate;


#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`
#define D3 21
#define D5 22
// Initialize the OLED display using Wire library
SSD1306Wire  display(0x3c, D3, D5, GEOMETRY_128_32);


int ww = 128;
int hh = 32; //64; //32;

int scale = 1;



#ifndef DoubleVClass
#include "DoubleV.h"
#endif

#ifndef ObjectClass
#include "Object.h"
#endif

#ifndef GameStuffClass
#include "GameStuff.h"
#endif


void setup()
{
  pinMode(2, OUTPUT);
  Serial.begin(115200);
  //delay(1000); // give me time to bring up serial monitor
  Serial.println("ESP32 Touch Test");


  // Initialising the UI will init the display too.
  display.init();

  // display.flipScreenVertically();
  //display.setFont(ArialMT_Plain_10);
  display.setFont(ArialMT_Plain_24);

  Serial.println(display.getWidth());
  Serial.println(display.getHeight());



  gameInit();

}

unsigned int testTouch, lastTouch;
void loop()
{
  for (int i = 0; i < 10; i ++)
  {
    if (i != 2)
    {
      touchValue[i] = touchRead(touchPin[i]);
    }
    if (touchValue[i] <= threshold)
      touched[i] = true;
    else
      touched[i] = false;

    //if (testTouch != lastTouch)
    // Serial.print(testTouch);  // get value using T0
    // Serial.print(", ");
    // lastTouch = testTouch;
  }

  rightPressed = touched[3]; // bottom Right;
  leftPressed = touched[4]; // bottom Left;
  wPressed = touched[6];
  sPressed = touched[0];

  if (touched[6] == true)
    digitalWrite(2, HIGH);
  else
    digitalWrite(2, LOW);


  //Serial.println();

  display.clear();

  /*
    if (touched[0]) // top Right
      display.setColor(WHITE);
    else
      display.setColor(BLACK);
    display.fillRect(128 - 26, 0, 26, 26);

    if (touched[3]) // bottom Right
      display.setColor(WHITE);
    else
      display.setColor(BLACK);
    display.fillRect(128 - 26, 64 - 26, 26, 26);

    if (touched[4]) // bottom Left
      display.setColor(WHITE);
    else
      display.setColor(BLACK);
    display.fillRect(0, 64 - 26, 26, 26);

    if (touched[6]) // Top Left
      display.setColor(WHITE);
    else
      display.setColor(BLACK);
    display.fillRect(0, 0, 26, 26);
  */
  display.setColor(WHITE);
  gameLoop();

  display.display();




  //delay(50);
  static unsigned long lastMicros = micros();
  while (micros() - lastMicros < framePeriod * 1000000)
  {
  }
  lastMicros = micros();
}






/*
  void keyPressed()
  {
  switch (keyCode)
  {
    case RIGHT:
      rightPressed = true;
      break;
    case LEFT:
      leftPressed = true;
      break;
  }
  switch (key)
  {
    case 'W':
    case 'w':
      wPressed = true;
      break;
    case 'S':
    case 's':
      sPressed = true;
      break;
  }
  }

  void keyReleased()
  {
  switch (keyCode)
  {
    case RIGHT:
      rightPressed = false;
      break;
    case LEFT:
      leftPressed = false;
      break;
  }
  switch (key)
  {

    case 'W':
    case 'w':
      wPressed = false;
      break;
    case 'S':
    case 's':
      sPressed = false;
      break;
  }
  }
*/
