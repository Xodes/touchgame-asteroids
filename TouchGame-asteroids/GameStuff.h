#ifndef GameStuffClass
#define GameStuffClass 1

#ifndef DoubleVClass
#include "DoubleV.h"
#endif

#ifndef ObjectClass
#include "Object.h"
#endif

Object player = Object(0);
Object bullets[20];

void gameInit()
{

  for (int i = 0; i < 20; i++)
  {
    bullets[i] = Object(1);
    bullets[i].health = 0;
  }


  player.objectType = 0;

  for (int i = 0; i < 5; i++)
  {
    playerPoints[i] = DoubleV(0, 0);
  }
  playerPoints[0].setV(0, 1.4);
  playerPoints[1].setV(0, -1.4);
  playerPoints[2].setV(4.4, 0);
  playerPoints[3].setV(-1.4, -1.4);
  playerPoints[4].setV(-1.4, 1.4);

  player.pos = DoubleV(32, 16);
}

void gameLoop()
{

  if (wPressed && !lastwPressed)
  {
    // fire
    int bulletCounter = 0;
    while (bullets[bulletCounter].health > 0)
    {
      bulletCounter++;
      if (bulletCounter >= 20)
      {
        //bulletCounter = 20;
        break;
      }
    }
    if (bulletCounter < 20)
    {
      bullets[bulletCounter] = Object(1);
      bullets[bulletCounter].pos.setV(player.pos);
      bullets[bulletCounter].velocity = DoubleV(60.0, 0).rotate(player.rot).add(player.velocity);
    }
  }

  for (int i = 0; i < 20; i++)
  {
    bullets[i].update();
    bullets[i].draw();
  }


  //keyPressedCheck
  //player.rotSpeed = 2 * PI / 20 / 10;
  player.update();
  player.draw();
  //player.rot += 2*PI/20/10;

  lastwPressed = wPressed;
}

#endif
