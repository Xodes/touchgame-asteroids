#ifndef DoubleVClass
#define DoubleVClass 1



//template< typename T, byte ArraySize >
class DoubleV
{

  public:
    double x = 0.0, y = 0.0;

    DoubleV()
    {
      x = 0.0;
      y = 0.0;
    }

    DoubleV(double xx, double yy)
    {
      x = xx;
      y = yy;
    }

    DoubleV& setV(double xx, double yy)
    {
      x = xx;
      y = yy;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }

    DoubleV& setV(DoubleV DV)
    {
      x = DV.x;
      y = DV.y;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }


    DoubleV& getV()
    {
      return *this; // will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV clone()
    {
      return DoubleV(x, y); // will return a copy of the vector
    }

    DoubleV& add(double zz)
    {
      x += zz;
      y += zz;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& add(double xx, double yy)
    {
      x += xx;
      y += yy;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& add(DoubleV zz)
    {
      x += zz.x;
      y += zz.y;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }

    DoubleV& sub(double zz)
    {
      x -= zz;
      y -= zz;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& sub(double xx, double yy)
    {
      x -= xx;
      y -= yy;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& sub(DoubleV zz)
    {
      x -= zz.x;
      y -= zz.y;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }

    DoubleV& mult(double zz)
    {
      x *= zz;
      y *= zz;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& mult(double xx, double yy)
    {
      x *= xx;
      y *= yy;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& mult(DoubleV zz)
    {
      x *= zz.x;
      y *= zz.y;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }

    DoubleV& divide(double zz)
    {
      x /= zz;
      y /= zz;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& divide(double xx, double yy)
    {
      x /= xx;
      y /= yy;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& divide(DoubleV zz)
    {
      x /= zz.x;
      y /= zz.y;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }

    double mag()
    {
      return sqrt( pow(x, 2) + pow(y, 2) );
    }

    DoubleV sign()
    {
      int xs, ys;
      if (x == 0)
        xs = 0;
      else if (x > 0)
        xs = 1;
      else
        xs = -1;

      if (y == 0)
        ys = 0;
      else if (y > 0)
        ys = 1;
      else
        ys = -1;

      return DoubleV(xs, ys);
    }
    int signX()
    {
      int xs;
      if (x >= -0.000001 && x <= 0.000001)
        xs = 0;
      else if (x > 0.0)
        xs = 1;
      else
        xs = -1;

      return xs;
    }
    int signY()
    {
      int ys;
      if (y >= -0.000001 && y <= 0.000001)
        ys = 0;
      else if (y > 0.0)
        ys = 1;
      else
        ys = -1;

      return ys;
    }

    DoubleV& normalize()
    {
      double magnitude = mag();
      x /= magnitude;
      y /= magnitude;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV normals()
    {
      double xNorm, yNorm;
      double magnitude = mag();
      xNorm = x / magnitude;
      yNorm = y / magnitude;
      return DoubleV(xNorm, yNorm);
    }
    DoubleV& setNormals(double xx, double yy)
    {
      double magnitude = mag();
      x = xx * magnitude;
      y = yy * magnitude;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& setNormals(DoubleV zz)
    {
      double magnitude = mag();
      x = zz.normals().x * magnitude;
      y = zz.normals().y * magnitude;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& setMag(double zz)
    {
      DoubleV norms = normals();
      x = norms.x * zz;
      y = norms.y * zz;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }
    DoubleV& setMag(DoubleV zz)
    {
      double magnitude = zz.mag();
      DoubleV norms = normals();
      x = norms.x * magnitude;
      y = norms.y * magnitude;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }


    DoubleV& rotate(double angle) // rotates about the origion (0,0)
    {
      DoubleV tempV;
      tempV.x = x * cos(angle) - y * sin(angle);
      tempV.y = x * sin(angle) + y * cos(angle);
      x = tempV.x;
      y = tempV.y;
      return *this; // once set, will return the exact copy of the vector, incase some other function wants to change it or use it in the same line.
    }

    double getAngle() // gives you the tan Angle // gets you the arccos
    {
      double angle = acos(x / mag() );
      if (y < 0)
        angle *= -1;
      return angle;
    }

    DoubleV& setAngle(double angle)
    {
      double tempMag = mag();
      x = tempMag * cos((float)angle);
      y = tempMag * sin((float)angle);
      return *this;
    }


    DoubleV& roundV()
    {
      //x = round((float)x);
      //y = round((float)y);
      long intX = long(x + 0.5);
      long intY = long(y + 0.5);
      x = double(intX);
      y = double(intY);
      return *this;
    }

    DoubleV& ceilV()
    {
      //x = round((float)x);
      //y = round((float)y);
      long intX = long(x);
      long intY = long(y);

      if (x > double(intX))
        intX ++;
      x = double(intX);

      if (y > double(intY))
        intY ++;
      y = double(intY);
      return* this;
    }

    DoubleV& floorV()
    {
      //x = round((float)x);
      //y = round((float)y);
      long intX = long(x);
      long intY = long(y);
      x = double(intX);
      y = double(intY);
      return* this;
    }

    // do i add a dist or lerp function? or do i do  vc=va.clone().sub(vb); ?
};


double roundD(double inNum)
{
  long intNum = (long)(inNum + 0.5);
  inNum = (double)intNum;
  return inNum;
}

double ceilD(double inNum)
{
  long intNum = (long)(inNum + 0.5);
  if (inNum > (double)intNum)
    intNum ++;
  inNum = (double)intNum;
  return inNum;
}

double floorD(double inNum)
{
  long intNum = (long)(inNum);
  inNum = (double)intNum;
  return inNum;
}


//// a swap function used in shiftoMp();
//template< typename T, byte ArraySize >
//void MedianFilter<T, ArraySize>::swapoMp( byte a, byte b)
//{
//  byte temp = mpOrder[b];
//  mpOrder[b] = mpOrder[a];
//  mpOrder[a] = temp;
//}

#endif
